<!-- Avant de poster un ticket, vérifiez qu'un ticket similaire ne concerne pas déjà le même sujet.
Si c'est le cas, ajoutez plutôt votre message à sa suite pour donner des précisions. -->

<!-- Les notifications aux réponses seront reçues sur votre boîte mail académique. -->

<!-- Avant de rapporter ce bug, avez-vous vérifié que :
- vous utilisez bien un fichier ORACE téléchargé ici ?
En effet, il existe des versions "parallèles" de ORACE et le fichier qui vous pose problème ne vient peut-être pas d'ici.
- vous utilisez la version recommandée de LibreOffice ?
En effet, une version trop ancienne risque de poser problème.
- vous utilisez la DERNIÈRE version du fichier ORACE correspondant aux évaluations traitées ?
- vous avez bien abaissé le niveau de sécurité des macros dans LibreOffice à "Moyen" ?
- le fichier des résultats de la circonscription "suivi_ien_xxx.xls" est complet et non corrompu ? -->

## Description du problème
<!-- Décrivez en une ou deux phrases ce qui se passe. -->


## Étapes pour reproduire
<!-- Indiquez les étapes à suivre pour constater le bug. -->

1. 
2. 
3. 

## Résultat attendu
<!-- À quoi devrait-on s'attendre à le suite de ces étapes ? -->


## Résultat constaté
<!-- Que se passe-t-il à la place ? -->


## Configuration
<!-- Sur quel système (Linux, Windows ...), quel navigateur (Firefox, Chrome...) avez-vous testé ? -->


## Informations supplémentaires
<!-- Ajoutez si nécessaire des captures d'écran ou une vidéo, et/ou des logs.-->