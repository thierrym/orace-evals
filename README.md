Page publique de ce projet : [https://thierrym.forge.apps.education.fr/orace-evals/](https://thierrym.forge.apps.education.fr/orace-evals/).

Dépôt des différentes versions du classeur ORACE fonctionnant sous LibreOffice Calc. Ce classeur traite les résultats aux évaluations CP, CE1 et CM1 (depuis septembre 2023) et aussi CE2 et CM2 (depuis septembre 2024) d'une circonscription à partir de l'extraction des résultats bruts via [https://evaluations-reperes.fr](https://evaluations-reperes.fr) et le bouton "Télécharger le rapport IEN".

Ce classeur a été créé initialement par Cyrille MARTIN, ERUN de la circonscription de Cholet et Montrevault (49) dans l'académie de Nantes et a été ensuite maintenu par Élisa PETIT, ERUN de la circonscription d'Ussel - Haute Corrèze (19) dans l'académie de Limoges. Depuis septembre 2022, ce document est actualisé, amélioré, corrigé par Thierry Munoz, ERUN de la circonscription Lézignan, Corbières et Minervois (11) via ce dépôt Gitlab.

Il existe une version pour les évaluations de septembre et une version différente pour celles de mi-CP au mois de janvier.  
**Les classeurs ORACE sont spécifiques à chaque évaluation** : le classeur de septembre 2024 ne fonctionnera que pour les évaluations de septembre 2024. Si vous avez besoin d'une version antérieure, vous la retrouverez dans le dossier "Archives".

- ATTENTION, **les 2 classeurs ci-dessous est à destination des circonscriptions** pour traiter l'unique fichier xls regroupant l'ensemble des résultats de leurs écoles :  
    - Version actuelle pour les évaluations de septembre 2024 : [ORACE_maj2024_sept2024_v9.ods](ORACE_maj2024_sept2024_v9.ods?inline=false)
    - Version actuelle pour les évaluations Mi-CP de janvier 2025 : [ORACE_maj2025_janvier2025_v4.ods](ORACE_maj2025_janvier2025_v4.ods?inline=false)
- ATTENTION, **le classeur ci-dessous est à destination des directeurs et directrices d'école** pour synthétiser les résultats de leur seule école à partir de leur nombreux fichiers xls obtenus via le menu "Classes" sur le site [https://evaluations-reperes.fr](https://evaluations-reperes.fr) avec le code "directeur"!!!  
    - Version actuelle de ORACE_École pour les évaluations Mi-CP de janvier 2025 : [Orace_École_Mi-CP_janvier_2025_v3.ods](Orace_École_Mi-CP_janvier_2025_v3.ods?inline=false)


**Nouveautés Septembre 2024** :

- [Classeur Moulinette Rapport Progression Saisie](Moulinette_Rapport_Progression_%C3%89vals_Circo_Vierge.ods?inline=false) pour suivre l'état des saisies des écoles sur le site https://evaluations-reperes.fr/ à partir du rapport d'activité / progression du compte de la circonscription. Toutes les informations d'utilisation sont incluses dans le classeur. Contrairement aux classeurs ORACE, celui-ci est utilisable pour toutes les évaluations.
![Capture d'écran](Images/Capture_Rapport_Activité.png)

- Nouveau classeur permettant de traiter les résultats aux **évaluations de septembre 2024** au niveau d'une école en fusionnant tous les fichiers xls en un seul. En effet, les réultats que recueille l'école sont éclatés en de multiples fichier .xls (autant de fichiers par niveaux et par matière pour une seule classe, ce qui parfois représente plus de 50 fichiers !!!). On obtient ainsi des tableaux regroupant tous les élèves sur une seule feuille pour un niveau et la matière données, des graphes au niveau de l'école ainsi qu'au niveau de chaque classe (avec exportation au format pdf pour chaque classe).

## AVERTISSEMENT version LibreOffice
**Le dernier fichier proposé pour ORACE a été testé sous LIBREOFFICE 28.4.1 -> Version recommandée !!!**

## En cas de problème ?
Avez-vous vérifié que :

- vous utilisez bien un fichier ORACE téléchargé ici ?  
En effet, il existe des versions "parallèles" de ORACE et le fichier qui vous pose problème ne vient peut-être pas d'ici.

- vous utilisez la version LibreOffice recommandée ?  
En effet, une version trop ancienne risque de poser problème.

- vous utilisez la **dernière** version du fichier ORACE correspondant aux évaluations traitées ?

- vous avez bien abaissé le niveau de sécurité des macros dans LibreOffice à "Moyen" ?  
Si c'est le cas lors de l'ouverture de ORACE vous devriez avoir un message d'avertissement vous demandant d'activer ou non les macro. ORACE nécessite d'activer les macro pour fonctionner. Par contre, les macro présentant un risque potentiel de sécurité, vérifiez bien que le fichier ORACE vient de ce dépôt !!!
Pour savoir comment faire : [https://www.lofurol.fr/joomla/logiciels-libres/libreoffice/150-libreoffice-macros-securite-activation-programmation](https://www.lofurol.fr/joomla/logiciels-libres/libreoffice/150-libreoffice-macros-securite-activation-programmation)

- le fichier des résultats de la circonscription "suivi_ien_xxx.xls" est complet et non corrompu ?

En dehors de cette page, vous pouvez trouver d'autres infos dans le [wiki](https://forge.apps.education.fr/thierrym/orace-evals/-/wikis/home).

Si après toutes ces vérifications, ça ne marche toujours pas, vous pouvez déposer un ticket en suivant ce lien : [https://forge.apps.education.fr/thierrym/orace-evals/-/issues](https://forge.apps.education.fr/thierrym/orace-evals/-/issues/new?issuable_template=bug)
